﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Collections;

namespace WindowsFormsApplication2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string text=""; //хранится обрабатывемый код
        Lexem[] MasLex = new Lexem[100];


         public struct Lexem
         {
             public string lex;          //Лексема
             public string type;         //тип лексемы 
             public string reg;          //регуляроное выражение
         }

         public struct LexemResult
         {
             public string lex;          //Лексема
             public string type;         //тип лексемы 
             public string description;  //описание в коде
         }

          //заполняем массив регулярных выражений
          private void Form1_Load(object sender, EventArgs e)
          {

              MasLex[0].lex = "X1";
              MasLex[0].type = "Ключевое слово";
              MasLex[0].reg = @"if";

              MasLex[1].lex = "X2";
              MasLex[1].type = "Ключевое слово";
              MasLex[1].reg = @"then";

              MasLex[2].lex = "X3";
              MasLex[2].type = "Ключевое слово";
              MasLex[2].reg = @"else";

              MasLex[3].lex = "Y1";
              MasLex[3].type = "Разделитель";
              MasLex[3].reg = @";";

              MasLex[4].lex = "Y2";
              MasLex[4].type = "Знак сравнения";
              MasLex[4].reg = @"<";

              MasLex[5].lex = "Y3";
              MasLex[5].type = "Знак сравнения";
              MasLex[5].reg = @">";

              MasLex[6].lex = "Y4";
              MasLex[6].type = "Знак сравнения";
              MasLex[6].reg = @"=";

              MasLex[7].lex = "NUM";
              MasLex[7].type = "Целочисленная константа";
              MasLex[7].reg = @"(-)?\d+";

              MasLex[8].lex = "NUM";
              MasLex[8].type = "Вещественная константа";
              MasLex[8].reg = @"(-)?[0-9]*\.*[0-9]+";

              MasLex[9].lex = "NUM";
              MasLex[9].type = "Логарифмическая форма";
              MasLex[9].reg = @"(((-)?\d+[0-9]*\.*[0-9]+(e|e-)\d+)|((-)?\d+[0-9]*\.*[0-9]+(E|E-)\d+))";

              MasLex[10].lex = "Y5";
              MasLex[10].type = "Знак присваивания";
              MasLex[10].reg = @":=";

              MasLex[11].lex = "N";
              MasLex[11].type = "Идентификатор";
              MasLex[11].reg = @"[a-zA-Z0-9_]*";

          }

        //regex
        private void button1_Click(object sender, EventArgs e)
        {
            text = "";
            string[] lines = richTextBox1.Lines;

            foreach (string t in lines)
            {
                text = text + " " + t;
            }

            text = text.Replace(">", " > ");
            text = text.Replace("<", " < ");
            text = text.Replace(";", " ; ");
            text = text.Replace(":=", " := ");
            text = text.Replace("=", " = ");
            text = text.Replace(": =", " := ");

            int count_N = 0;
            string temp_lex = "";

            ArrayList ListLex=new ArrayList();//результат

            LexemResult temp = new LexemResult();
            temp.description = "";
            temp.lex = "";
            temp.type = "";

            int leng = 0;

            string [] split = text.Split(new Char [] {' '});

            foreach (string s in split)
            {
                if (s.Trim() != "")
                {
                    for (int i = 0; i < 12; i++)
                    {
                        if (Regex.Match(s, MasLex[i].reg).Value.Length>leng) 
                        {
                            leng = Regex.Match(s, MasLex[i].reg).Value.Length;
                            temp.lex = MasLex[i].lex;
                            temp.type = MasLex[i].type;
                            temp.description = Regex.Match(s, MasLex[i].reg).Value;
                        
                        }
                    }

                    leng = 0;

                    //блок создания таблицы разбора
                    if (temp.lex == "NUM")
                        temp.lex = temp.description;

                    if (temp.lex == "N")
                    {
                        foreach (LexemResult result in ListLex)
                        {
                            if (temp.description == result.description)
                            {
                                temp_lex = result.lex;
                                break;
                            }
                        }

                        if (temp_lex != "")
                            temp.lex = temp_lex;
                        else
                            temp.lex = "N" + ++count_N;

                        temp_lex = "";
                    }


                    if (temp.type == "")
                    {
                        temp.description = s;
                        temp.type = "ОШИБКА";
                        ListLex.Add(temp);
                    }
                    else
                        ListLex.Add(temp);

                    temp.description = "";
                    temp.lex = "";
                    temp.type = "";
                }
                
            }

            int j = 0;

            dataGridView1.Rows.Clear();
                        
            foreach (LexemResult result in ListLex)
            {
                dataGridView1.Rows.Add();
                dataGridView1.Rows[j].Cells[0].Value = result.description;
                dataGridView1.Rows[j].Cells[1].Value = result.type;
                dataGridView1.Rows[j].Cells[2].Value = result.lex;
                j++;
            }
        }

        //автомат
        private void button3_Click(object sender, EventArgs e)
        {
            text = "";
            string[] lines = richTextBox1.Lines;

            foreach (string t in lines)
            {
                text = text + " " + t;
            }
            text = text.Replace(">", " > ");
            text = text.Replace("<", " < ");
            text = text.Replace(";", " ; ");
            text = text.Replace(":=", " := ");
            text = text.Replace("=", " = ");
            text = text.Replace(": =", " := ");


            ArrayList ListLex = new ArrayList();//результат

            ListLex.Clear();

            LexemResult temp = new LexemResult();
            temp.description = "";
            temp.lex = "";
            temp.type = "";

            int count_N = 0;
            string temp_lex = "";

            string ZNACH = "";
            string slovo = "";
            string[] split = text.Split(new Char[] { ' ' });

            foreach (string s in split)
            {
                ZNACH = "";
                slovo = s + " ";
                for (int i = 0; i < slovo.Length; i++)
                {
                    if (ZNACH == "" & slovo[i] == 'i')
                        ZNACH = "k1";

                    if (ZNACH == "k1" & slovo[i] == 'f')
                        ZNACH = "k2";

                    if (ZNACH == "k2" & slovo[i] == ' ')
                        ZNACH = "-1";

                    //////////////////////////

                    if (ZNACH == "" & slovo[i] == 't')
                        ZNACH = "k3";

                    if (ZNACH == "k3" & slovo[i] == 'h')
                        ZNACH = "k4";

                    if (ZNACH == "k4" & slovo[i] == 'e')
                        ZNACH = "k5";

                    if (ZNACH == "k5" & slovo[i] == 'n')
                        ZNACH = "k6";

                    if (ZNACH == "k6" & slovo[i] == ' ')
                        ZNACH = "-2";
                   
                    //////////////////////////

                    if (ZNACH == "" & slovo[i] == 'e')
                        ZNACH = "k7";

                    if (ZNACH == "k7" & slovo[i] == 'l')
                        ZNACH = "k8";

                    if (ZNACH == "k8" & slovo[i] == 's')
                        ZNACH = "k9";

                    if (ZNACH == "k9" & slovo[i] == 'e')
                        ZNACH = "k10";

                    if (ZNACH == "k10" & slovo[i] == ' ')
                        ZNACH = "-3";

                    //////////////////////////

                    if (ZNACH == "" & slovo[i] == ':')
                        ZNACH = "p1";

                    if (ZNACH == "p1" & slovo[i] == '=')
                        ZNACH = "p2";

                    if (ZNACH == "p2" & slovo[i] == ' ')
                        ZNACH = "-4";

                    /////////////////////////

                    if (ZNACH == "" & slovo[i] == ';')
                        ZNACH = "r1";

                    if (ZNACH == "r1" & slovo[i] == ' ')
                        ZNACH = "-5";

                    /////////////////////////

                    if (ZNACH == "" & slovo[i] == '<')
                        ZNACH = "z1";

                    if (ZNACH == "z1" & slovo[i] == ' ')
                        ZNACH = "-6";

                    /////////////////////////

                    if (ZNACH == "" & slovo[i] == '>')
                        ZNACH = "z2";

                    if (ZNACH == "z2" & slovo[i] == ' ')
                        ZNACH = "-7";

                    /////////////////////////

                    if (ZNACH == "" & slovo[i] == '=')
                        ZNACH = "z3";

                    if (ZNACH == "z3" & slovo[i] == ' ')
                        ZNACH = "-8";

                    /////////////////////////

                    if (ZNACH == "" & (slovo[i] == '-' | Char.IsDigit((slovo[i]))))
                        ZNACH = "num1";

                    if (ZNACH == "num1" & slovo[i] == ' ')
                        ZNACH = "-9";

                    if (ZNACH == "num1" & Char.IsDigit((slovo[i])))
                        ZNACH = "num";

                    if (ZNACH == "num" & slovo[i] == ' ')
                        ZNACH = "-9";

               /*    if (i + 1 < slovo.Length)
                    {
                        if ((ZNACH == "num1" | ZNACH == "num") & Char.IsDigit((slovo[i + 1])) == false & slovo[i + 1] != '.' & slovo[i + 1] != ' ' & slovo[i + 1] != 'e')
                            ZNACH = "-13";
                    } */

                    /////////////////////////

                    if (ZNACH == "num" & slovo[i] == '.')
                        ZNACH = "fract1";

                    if (ZNACH == "fract1" & Char.IsDigit((slovo[i])))
                        ZNACH = "fract2";

                    if (ZNACH == "fract2" & slovo[i] == ' ')
                        ZNACH = "-10";

                 /*   if (i + 1 < slovo.Length)
                    {
                        if ((ZNACH == "fract1" | ZNACH == "fract2") & Char.IsDigit((slovo[i + 1])) == false & slovo[i + 1] != ' ')
                            ZNACH = "-13";
                    } */

                    /////////////////////////

                    if (ZNACH == "num" & slovo[i] == 'e')
                        ZNACH = "log1";

                    if (ZNACH == "fract2" & slovo[i] == 'e')
                        ZNACH = "log1";

                    if (ZNACH == "log1" & (slovo[i] == '-' | Char.IsDigit((slovo[i]))))
                        ZNACH = "log2";

                    if (ZNACH == "log2" & Char.IsDigit((slovo[i])))
                        ZNACH = "log2";

                    if (ZNACH == "log2" & slovo[i] == ' ')
                        ZNACH = "-11";

                /*   if (i + 1 < slovo.Length)
                    {
                        if (ZNACH == "log2" & Char.IsDigit((slovo[i + 1])) == false & slovo[i + 1] != ' ' & slovo[i + 1] != '-' & slovo[i + 1] != 'e')
                            ZNACH = "-13"; 
                    } */

                    //////////////////////////

                    if (ZNACH == "" & (Char.IsLetter(slovo[i]) | slovo[i] == '_'))
                        ZNACH = "id1";

                    if (ZNACH == "id1" & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                        ZNACH = "id1";

                    if (i + 1 < slovo.Length)
                    {
                        if (ZNACH == "k1" & slovo[i + 1] != 'f' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k2" & slovo[i + 1] != ' ' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k3" & slovo[i + 1] != 'h' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k4" & slovo[i + 1] != 'e' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k5" & slovo[i + 1] != 'n' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k6" & slovo[i + 1] != ' ' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k7" & slovo[i + 1] != 'l' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k8" & slovo[i + 1] != 's' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k9" & slovo[i + 1] != 'e' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";

                        if (ZNACH == "k10" & slovo[i + 1] != ' ' & (Char.IsLetter(slovo[i]) | slovo[i] == '_' | Char.IsDigit((slovo[i]))))
                            ZNACH = "id1";
                    }

                    if (ZNACH == "id1" & slovo[i] == ' ')
                        ZNACH = "-12";
                }

               if (ZNACH != "-1" & ZNACH != "-2" & ZNACH != "-3" & ZNACH != "-4" & ZNACH != "-5" & ZNACH != "-6" & ZNACH != "-7" &
                  ZNACH != "-8" & ZNACH != "-9" & ZNACH != "-10" & ZNACH != "-11" & ZNACH != "-12")
                  ZNACH = "-13";

                temp.description=slovo;

                if (ZNACH == "-1")
                {
                    temp.type = "Ключевое слово";
                    temp.lex = "X1";
                }
                if (ZNACH == "-2")
                {
                    temp.type = "Ключевое слово";
                    temp.lex = "X2";
                }
                if (ZNACH == "-3")
                {
                    temp.type = "Ключевое слово";
                    temp.lex = "X3";
                }
                if (ZNACH == "-4")
                {
                    temp.type = "Присваивание";
                    temp.lex = "Y5";
                }
                if (ZNACH == "-5")
                {
                    temp.type = "Разделитель";
                    temp.lex = "Y1";
                }
                if (ZNACH == "-6")
                {
                    temp.type = "Знак сравения";
                    temp.lex = "Y2";
                }
                if (ZNACH == "-7")
                {
                    temp.type = "Знак сравения";
                    temp.lex = "Y3";
                }
                if (ZNACH == "-8")
                {
                    temp.type = "Знак сравения";
                    temp.lex = "Y4";
                }
                if (ZNACH == "-9")
                {
                    temp.type = "Целое число";
                    temp.lex = "NUM";
                }
                if (ZNACH == "-10")
                {
                    temp.type = "Вещественное";
                    temp.lex = "NUM";
                }
                if (ZNACH == "-11")
                {
                    temp.type = "Логарифмическая форма числа";
                    temp.lex = "NUM";
                }
                if (ZNACH == "-12")
                {
                    temp.type = "Идентификатор";
                    temp.lex = "N";
                }
                if (ZNACH=="-13")
                    temp.type="Ошибка";


                //блок создания таблицы разбора
                if (temp.lex == "NUM")
                    temp.lex = temp.description;

                if (temp.lex == "N")
                {
                    foreach (LexemResult result in ListLex)
                    {
                        if (temp.description == result.description)
                        {
                            temp_lex = result.lex;
                            break;
                        }
                    }

                    if (temp_lex != "")
                        temp.lex = temp_lex;
                    else
                        temp.lex = "N" + ++count_N;

                    temp_lex = "";
                }

                if ( temp.description!=" ")
                ListLex.Add(temp);

                ZNACH = "";

                slovo = "";

                temp.description = "";
                temp.lex = "";
                temp.type = "";

            }
            
            int j=0;

            dataGridView2.Rows.Clear();

            foreach (LexemResult result in ListLex)
            {
                dataGridView2.Rows.Add();
                dataGridView2.Rows[j].Cells[0].Value = result.description;
                dataGridView2.Rows[j].Cells[1].Value = result.type;
                dataGridView2.Rows[j].Cells[2].Value = result.lex;
                j++;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            text = "";

            OpenFileDialog ofd = new OpenFileDialog();

            ofd.Filter= "Текстовый файл(*.txt)|*.*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                textBox1.Text = ofd.FileName;
                text= System.IO.File.ReadAllText(textBox1.Text);
                richTextBox1.Text = text;
            }

        }

        
    }
}
